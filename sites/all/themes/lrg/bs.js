var about;
var gallery;
var process;
var contact;

jQuery( document ).ready(function( $ ) {
	
	
	
 	jQuery('#next').click(function(x) {
	 	
    	x.preventDefault();
		jQuery('#gallery').data('backstretch').next();
		
	});

	jQuery('#previous').click(function(x) {

   	 	x.preventDefault();
   	 	jQuery('#gallery').data('backstretch').prev();

	});
	
	var page1 = document.getElementById("page1");
	page1.setAttribute("onclick", "closeMobileMenu();");
	
	var page2 = document.getElementById("page2");
	page2.setAttribute("onclick", "closeMobileMenu();");
	
	var page3 = document.getElementById("page3");
	page3.setAttribute("onclick", "closeMobileMenu();");
	
	var page4 = document.getElementById("page4");
	page4.setAttribute("onclick", "closeMobileMenu();");


	var path = location.pathname.lenght ? location.pathname : window.location.href;   
    $('.active').removeClass('active');    
    $("#fixed-menu-links").find('a[href*="'+path+'"]').addClass("active");
    
    
    
    about = $("#about").offset().top;
	gallery = $("#gallery").offset().top;
	process = $("#process").offset().top;
	contact = $("#contact").offset().top;

imageHover();
resizeThumbs();

	
});

jQuery(document).scroll(function(){
    
	// ABOUT-> less than gallery
	if (jQuery('body').hasClass('front')){
		var offset =  jQuery(this).scrollTop();
	
		if(jQuery(this).scrollTop() <= about)
	    {   
        	jQuery('#page1').css({"border-bottom":"1px solid #fff"});
			jQuery('#page2').css({"border-bottom":"0px solid #fff"});
			jQuery('#page3').css({"border-bottom":"0px solid #fff"});
			jQuery('#page4').css({"border-bottom":"0px solid #fff"});
     
    	}//  GALLERY -> More than about, less than process
		else if(jQuery(this).scrollTop() > about && jQuery(this).scrollTop() <= gallery)
		{   
        	jQuery('#page1').css({"border-bottom":"0px solid #fff"});
			jQuery('#page2').css({"border-bottom":"1px solid #fff"});
			jQuery('#page3').css({"border-bottom":"0px solid #fff"});
			jQuery('#page4').css({"border-bottom":"0px solid #fff"});
     
    	}// PROCESS-> more than gallery, less than contact
		else if(jQuery(this).scrollTop() > gallery && jQuery(this).scrollTop() <= process)
		{   
        	jQuery('#page1').css({"border-bottom":"0px solid #fff"});
			jQuery('#page2').css({"border-bottom":"0px solid #fff"});
			jQuery('#page3').css({"border-bottom":"1px solid #fff"});
			jQuery('#page4').css({"border-bottom":"0px solid #fff"});
		
    	}// CONTCAT more than process 
		else if(jQuery(this).scrollTop() >= process )
		{   
        	jQuery('#page1').css({"border-bottom":"0px solid #fff"});
			jQuery('#page2').css({"border-bottom":"0px solid #fff"});
			jQuery('#page3').css({"border-bottom":"0px solid #fff"});
			jQuery('#page4').css({"border-bottom":"1px solid #fff"});
     
    	}
    
    }
    
    
});

function closeMobileMenu(){
	
	var w = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
	
	if( w < 768){

		var resp_menu = document.getElementById("resp_menu");
		resp_menu.className = "responsive-menus responsive-menus-0-0 absolute responsified";
	}	
}

/*
var script = document.createElement('script');
script.src = 'http://code.jquery.com/jquery-3.1.1.min.js';
script.type = 'text/javascript';
document.getElementsByTagName('head')[0].appendChild(script);
*/

function imageHover(){
	var hoverDiv = jQuery('.field-name-field-lrg-gallery-collection .field-item');	
	hoverDiv.mouseenter(function(e){
		e.preventDefault();
		var imageDisplay = jQuery(this).find('li a.fancybox');
		var galleryTitle = imageDisplay.attr('title');
		
		var newDiv = '<div class="tag"><h4 class="top">' + galleryTitle + '</h4></div>';
		
		imageDisplay.append(newDiv);
		
		var tags = jQuery('.tag');
		if (tags.length > 1){
			for (var i = 1; i < tags.length; i++){ 
				tags[i].remove(); 
			};
		};
	});
	hoverDiv.mouseleave(function(e){
		e.preventDefault();
		jQuery("div.tag").remove();
	});
};

function resizeThumbs(){
	jQuery(window).resize(function(){
		if (jQuery(window).width() <= 475){	
			jQuery('.gallery-slides').css({
				'width' : '20rem',
				'height' : '200px'
			});
		
			jQuery('li a.fancybox').css({
				'width' : '20rem',
				'height' : '200px'
			});
		} else{
			jQuery('.gallery-slides').css({
				'width' : '31.5rem',
				'height' : '312px'
			});
		
			jQuery('li a.fancybox').css({
				'width' : '31.5rem',
				'height' : '312px'
			});
		}
	});	
};




jQuery(function($){
  if (!$('body.node-type-project-page').length) return;
  //... code specific to project pages

	
    jQuery('.field-label').hide();

		var thumbs = [];
		
		jQuery('.field-name-field-thumb').each(function(){
			thumbs.push(jQuery(this).find('img'));
		});
		
		for(i=0; i<thumbs.length; i++){
			thumbs[i]= thumbs[i].attr('src');
		};
		
		jQuery('.field-name-field-thumb').hide();
		
		jQuery('.group-right').append('<div id="thumbs-container"></div>');
		
		for (i=0; i<thumbs.length; i++){
			jQuery('#thumbs-container').append('<a href="#" class="pop"><div class="thumb-container"><img src="' + thumbs[i] + '" class="thumb" style="width: 100%; vertical-align: top;" /></div></a>');
		};
		
		var display = [];
		
		jQuery('.field-name-field-display').each(function(){
			display.push(jQuery(this).find('img'));
		});
		
		for(i=0; i<display.length; i++){
			display[i]= display[i].attr('src');
		};
		
		jQuery('.field-name-field-display').hide();
		
		jQuery('.node-project-page').prepend(
			'<div id="slider-container">' + 
				'<a href="#" class="control_prev">' + 
					'<img src="http://lrg.torquelaunchdev.com/sites/all/themes/lrg/theme-images/arrow-left.png">' + 
				'</a>' + 
				'<a href="#" class="control_next">' + 
					'<img src="http://lrg.torquelaunchdev.com/sites/all/themes/lrg/theme-images/arrow-right.png">' + 
				'</a>' + 
				'<div class="slider">' + 
				'</div>' + 
			'</div>'
		);
		
		jQuery('#slider-container .slider').append('<div class="slide" style="width: 100%;"></div>')
		
		var thisIndex = 0
		
		function displaySlide(){
			jQuery('.slide').html('<img src="' + display[thisIndex] + '" class="slide-img" style="width: 100%; vertical-align: top;">');
		}

		
		displaySlide();
		
		jQuery('a.control_prev').click(function(e){
								e.preventDefault();
								if (thisIndex === 0){
									thisIndex = (display.length - 1);
									displaySlide();
								} else {
									thisIndex = thisIndex - 1;
									displaySlide();
								}
							});
		
		jQuery('a.control_next').click(function(e){
						e.preventDefault();
						if (thisIndex === (display.length - 1) ){
							thisIndex = 0;
							displaySlide();
						} else {
							thisIndex = thisIndex + 1;
							displaySlide();
						}
					});
		
		jQuery('a.pop').click(function(event){
			event.preventDefault();
			thisIndex = jQuery('a.pop').index(jQuery(this));
			displaySlide();
		
		});
//   };
});



/*
	var thumbs = jQuery('.gallery-slides');
	
	for (var i = 0; i < thumbs.length; i++){
		var imageDiv = jQuery(thumbs[i]).find('li a.fancybox');
		var thumbnailLink = jQuery(thumbs[i]).find('li a.fancybox img').attr('src');
		var newDiv = '<div class="thumb" style="background-image: url(' + thumbnailLink + '); background-position: center center;"></div>';
		
		imageDiv.append(newDiv);
		jQuery('li a.fancybox img').hide();
	}
*/